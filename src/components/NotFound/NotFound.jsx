import React from 'react'
import img from '../../assets/banner/anastase-maragos-9dzWZQWZMdE-unsplash.jpg'
import { Link } from 'react-router-dom'
const NotFound = () => {
  return (
    <div className="flex flex-col items-center justify-center h-screen">
    <h1 className="text-4xl font-bold mb-4">404 - Not Found</h1>
    <p className="text-lg mb-8">Sorry, the page you are looking for does not exist.</p>
    <Link to='/'>
    <button className="btn btn-outline btn-info"> Back to Home</button>
    </Link>
    
  </div>

  )
}

export default NotFound