import React, { useContext } from 'react'
import logo from '../../assets/logo.png'
import { Link, NavLink } from 'react-router-dom'
import { AuthContext } from '../../provider/AuthProvider'
import auth from '../../config/firebase.config'
import toast from 'react-hot-toast'
import useBooking from '../../hooks/useBooking'
const Navbar = () => {
  const {user ,logOut ,isLoading}= useContext(AuthContext)
  const [bookings]= useBooking()
  const  handleSignOut= async()=>{
 
    try {
      await logOut(auth)
      
    } catch (error) {
      toast.error(error.message ,{id:toastId})
    }
  }
  return (
    <div>
        <div className="navbar bg-black py-5">
  <div className="navbar-start">
    <div className="dropdown">
      <label tabIndex={0} className="btn btn-ghost lg:hidden">
        <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h8m-8 6h16" /></svg>
      </label>
      <ul tabIndex={0} className="menu menu-sm dropdown-content mt-3 z-[1] p-2 shadow bg-base-100 rounded-box w-52  z-40">
      <li>
        <Link to='/'> Home</Link>
       </li>
      
       <li>
        <Link to='/classes'> Classes</Link>
       </li>
       <li>
        <Link to='/trainer'> Trainer</Link>
       </li>
       <li>
        <Link to='/bookings'> Bookings [{bookings.length} ]  </Link>
       </li>
       <li>
        <Link to='/dashboard/'> Dashoard   </Link>
       </li>
      
      {
        user?.email ? '' : <>  <li>
        <Link to='/login'>Login</Link>
       </li>
       <li>
        <Link to='/register'>Register</Link>
       </li></>
      }
      </ul>
    </div>
    <a className="btn btn-ghost text-xl">
        <img src={logo} alt="" className='w-48 h-12' />
    </a>
  </div>
  <div className="navbar-center hidden lg:flex">
    <ul className="menu menu-horizontal px-1 text-white text-lg font-bold gap-3">
       <li>
        <Link to='/'> Home</Link>
       </li>
      
       <li>
        <Link to='/classes'> Classes</Link>
       </li>
       <li>
        <Link to='/trainer'> Trainer</Link>
       </li>
       <li>
        <Link to='/bookings'> Bookings [{bookings.length} ]  </Link>
       </li>
       <li>
        <Link to='/dashboard/'> Dashoard   </Link>
       </li>
      
      {
        user?.email ? '' : <>  <li>
        <Link to='/login'>Login</Link>
       </li>
       <li>
        <Link to='/register'>Register</Link>
       </li></>
      }
       
    </ul>
  </div>
  <div className="navbar-end">
    <Link className="btn btn-success text-white" to='/be-a-trainer'>Be a Trainer</Link>
  </div>
{
  user?.email ?   <div className="navbar-end z-50">
  <div className="flex-none">
   
    <div className="dropdown dropdown-end">
      <label tabIndex={0} className="btn btn-ghost btn-circle avatar">
        <div className="w-10 rounded-full">
          <img alt="Tailwind CSS Navbar component" src={user.photoURL} />
        </div>
      </label>
      <ul tabIndex={0} className="menu menu-sm dropdown-content mt-3 z-[1] p-2 shadow bg-base-100 rounded-box w-52">
        <li>
          <a className="justify-between">
           {user.displayName}
            <span className="badge">New</span>
          </a>
        </li>
      
        <li><Link onClick={handleSignOut}>Logout</Link></li>
      </ul>
    </div>
  </div>
  </div> :''
}
</div>
    </div>
  )
}

export default Navbar