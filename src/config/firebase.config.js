// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBawns_CC9tDJjKVgloCu9h1VxcOO8dmHQ",
  authDomain: "fitness-tracker-dd04d.firebaseapp.com",
  projectId: "fitness-tracker-dd04d",
  storageBucket: "fitness-tracker-dd04d.appspot.com",
  messagingSenderId: "745806545679",
  appId: "1:745806545679:web:8fc170cdab8c83dd32b47a"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig); 
const auth =getAuth(app)

export default auth


