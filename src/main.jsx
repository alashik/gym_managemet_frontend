import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import { RouterProvider } from 'react-router-dom'
import MainLayout from './Layout/MainLayout.jsx'
import router from './Routes/Routes';
import AuthProvider from './provider/AuthProvider.jsx'
import { Toaster } from 'react-hot-toast'
import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
import { HelmetProvider } from 'react-helmet-async'
const queryClient = new QueryClient();
ReactDOM.createRoot(document.getElementById('root')).render(
  <>
  <AuthProvider>
    <HelmetProvider>
    <QueryClientProvider   client={queryClient}>
     <RouterProvider router={router} />
     <Toaster />

    </QueryClientProvider>

    </HelmetProvider>
  </AuthProvider>
  </>,
)
