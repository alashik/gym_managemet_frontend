import React from 'react'
import { Link, NavLink, Outlet } from 'react-router-dom'
import { TiHome } from "react-icons/ti";
import { TbMoneybag } from "react-icons/tb";
import { IoMdAddCircle } from "react-icons/io";
import useAdmin from '../hooks/useAdmin';
import { Helmet } from 'react-helmet-async';
const DashBoard = () => {
  const [isAdmin ]=  useAdmin()
  return (
    <>
      <Helmet>
        <title>Fitness || DashBoard</title>
        <link rel="canonical" href="https://www.tacobell.com/" />
      </Helmet>
    <div className='flex'>
      <div className='w-64 min-h-screen bg-[#ddd]'>
            <ul className='menu flex space-y-4 mt-10 text-xl'>
          {
            isAdmin ? <>
               <li>
            <Link to="/">
              <span>
              <TiHome />
              </span>
                 Home
            </Link>
          </li>
               <li>
            <Link to="/dashboard/admin-home">
              <span>
              <TiHome />
              </span>
                Admin Home
            </Link>
          </li>
            <li>
            <Link to="/dashboard/users ">
              <span>
              <TiHome />
              </span>
                 All Users
            </Link>
          </li>
            <li>
            <Link to="/dashboard/all-trainers">
              <span>
              <TiHome />
              </span>
               All Trainer
            </Link>
          </li>
            <li>
            <Link to="/dashboard/applyTrainer">
              <span>
              <TiHome />
              </span>
                Apply trainer
            </Link>
          </li>
            </> :
             <> 
            <li>
            <Link to="/">
              <span>
              <TiHome />
              </span>
                  Home
            </Link>
            <Link to="/bookings">
              <span>
              <IoMdAddCircle />
              </span>
                 Booking
            </Link>
            <Link to="/dashboard/history">
              <span>
              <TbMoneybag />
              </span>
                 Payment History
            </Link>
          </li>
            </>
          }

         


        
            </ul>
      </div>
      <div className='flex-1 p-8'>
            <Outlet />
      </div>
    </div>
    </>
  )
}

export default DashBoard