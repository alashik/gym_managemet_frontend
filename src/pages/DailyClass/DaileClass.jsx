import React from 'react'

const DaileClass = ({ days, timeSlots, classes }) => {
  return (
    <div>
        <div className="min-w-fit lg:max-w-[1200px] mx-auto bg-white p-8 rounded shadow">
      <h1 className="text-4xl font-bold mb-6 text-center">Daily Classes Schedule</h1>
      <table className="w-full border-collapse border">
        <thead>
          <tr>
            <th className="p-4 border"></th>
            {timeSlots.map((slot, index) => (
              <th key={index} className="p-4 border">
                {slot}
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {days.map((day, index) => (
            <tr key={index}>
              <td className="p-4 border font-semibold">{day}</td>
              {timeSlots.map((slot, idx) => (
                <td key={idx} className="p-4 border">
                  {classes[day][slot] || '-'}
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
    </div>
    </div>
  )
}

export default DaileClass