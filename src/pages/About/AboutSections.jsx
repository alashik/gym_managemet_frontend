import React from "react";
import { FaCheckCircle } from "react-icons/fa";
import AboutCard from "./AboutCard";
const AboutSections = () => {
  return (
   <div className="bg-[#F6F6F6] py-24 ">
    <div className=" lg:w-[1200px] ;g:mx-auto px-5">
    <div className="lg:flex items-center gap-20 "> 
            <div className="lg:flex-1 space-y-5">
                <h3 className="text-[#87C232] text-2xl font-semibold"> WE'RE MAKING PEOPLE</h3>
                <h1 className="text-4xl font-extrabold mt-5">HEALTHIER, HAPPIER & <br /> STRONGER. JOIN US!</h1>
                <p className="text-xl text-gray-600 ">Powering Your Gym Experience</p>
            </div>
            <div className=" lg:flex-1 text-gray-600 leading-loose ">
                <p>We are confident that our staff and facility can help you to reach your fitness goals. Whether you’re a beginner or a pro, we’re here to help. After spending time in our facility and with our team we want our members to walk out of our doors after a workout and feel like they are a better version of themselves.</p>

                <div className="mt-5"> 
                    <p className="flex items-center gap-3"> <span className="text-xl  text-green-500"> <FaCheckCircle /> </span> <span> You will achieve your goals</span> </p>
                    <p className="flex items-center gap-3"> <span className="text-xl  text-green-500"> <FaCheckCircle /> </span> <span>You’ll gain confidence</span> </p>
                    <p className="flex items-center gap-3"> <span className="text-xl  text-green-500"> <FaCheckCircle /> </span> <span> You’ll learn something newYou’ll have unlimited support </span> </p>
                      
                   
                 
                </div>
            </div>
    </div>

    <AboutCard />
    </div>
      
   </div>
  );
};

export default AboutSections;
