import React from 'react'
import img1 from '../../assets/about/about-1.jpg'
 
const AboutCard = () => {
  return (
    <div className='lg:flex gap-20 mt-24'>
        <div className='flex-1'>
            <img src={img1} alt="" />
        </div>
        <div className='flex-1 space-y-5 leading-loose '>
            <h1 className='text-4xl font-medium text-[#87C232]'>LIFE IS MORE FUN</h1>
            <h1 className='text-4xl font-extrabold'>WHEN YOU FEEL FIT</h1>
            <p>Our gym was created by a group of personal trainers and fitness enthusiasts who wanted to get fit and healthy but were not satisfied with hidden fees and overcrowded gyms</p> 
            <p>We believe that fitness is not a hobby, but a way of life and want to share that with people who want to get fit and healthy.</p>
        </div>
    </div>
  )
}

export default AboutCard



 
