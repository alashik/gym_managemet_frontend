import { Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";
import React from "react";
import PaymentForm from "./PaymentForm";

const Payment = () => {
  const stripePromise = loadStripe(import.meta.env.VITE_STRIPE_PAYMENT);
  return (
    <div className=" ">
      <h1 className="text-center m-10 mb-10 text-3xl text-[#6d9b2e] font-extrabold italic">Make Payment  on Stripe </h1>
      <div>
        <Elements stripe={stripePromise}>
          < PaymentForm />
        </Elements>
      </div>
    </div>
  );
};

export default Payment;
