import React from 'react'
import useAuth from '../../hooks/useAuth'
import { useQuery } from '@tanstack/react-query'
import useAxiosSecure from '../../hooks/useAxiosSecure'
import { Helmet } from 'react-helmet-async'
 
 

const PaymentHistory = () => {
   const {user}= useAuth()
   const  axiosSecure= useAxiosSecure()

   const {data:payments=[]}= useQuery({
        queryKey: ['payments', user?.email],
        queryFn :async ()=>{
            const res = await axiosSecure.get(`/payments/${user?.email}`)
            return res.data   
             }
   })
   console.log(payments);
  return (
    <div>
        <Helmet>
        <title>Fitness ||  Payment History</title>
        <link rel="canonical" href="https://www.tacobell.com/" />
      </Helmet>
         <div className="overflow-x-auto">
        <table className="table">
          {/* head */}
          <thead>
            <tr>
              <th>#</th>
              <th>Transaction Id</th>
              <th>Email</th>
              <th>Date</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            {payments.map((pay, i) => (
              <tr  >
                <th> {i + 1}</th>
                <td>{pay.email}</td>
                <td>{pay.date}</td>
                <td>{pay.transactionId}</td>
                <td>{pay.status}</td>
               
              </tr>
            ))}
          </tbody>
        </table>
      </div> 
    </div>
  )
}

export default PaymentHistory