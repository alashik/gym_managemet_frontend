import { CardElement, useElements, useStripe } from '@stripe/react-stripe-js';
import React, { useEffect, useState } from 'react'
import useAuth from '../../hooks/useAuth';
import useAxiosSecure from './../../hooks/useAxiosSecure';
import useBooking from '../../hooks/useBooking';
import toast from 'react-hot-toast';
import { useNavigate } from 'react-router-dom';
 


const PaymentForm = () => {
    const navigate=useNavigate()
    const [error ,setError]= useState('')
    const {calculateSum , user} = useAuth()
    const price=calculateSum()
    const stripe = useStripe();
    const elements = useElements();
    const axiosSecure = useAxiosSecure()
    const [clientSecret, setClientSecret] = useState("");
    const [transactionId ,setTransactionId]= useState('')
     console.log(typeof price);
     const [bookings]= useBooking() 
     console.log(bookings);

    useEffect(()=>{
      if(price >0){
        axiosSecure.post('/create-payment-intent', {price})
        .then(res=>{
            setClientSecret(res.data.clientSecret)
        })
      }
    },[])
    
    const handleSubmit= async(e)=>{
        e.preventDefault();

        if (!stripe || !elements) {
           
            return;
          }

        const card = elements.getElement(CardElement);

        if (card == null) {
            return;
          }
        
          const {error, paymentMethod} = await stripe.createPaymentMethod({
            type: 'card',
            card,
          });

          if (error) {
            console.log('[error]', error);
            setError(error.message)
          } else {
            console.log('[PaymentMethod]', paymentMethod);
            setError('')
          }

          // confirm payments
        const {paymentIntent, error:confirmError}= await stripe.confirmCardPayment(clientSecret,{
            payment_method: {
                card: card,
                billing_details: {
                email :user?.email || 'Anonymous',
                name: user?.displayName || 'Anonymous',
                },
              },
          })

          if(confirmError){
            console.log('Confirm error');
          }else{
            console.log('payment intent' ,paymentIntent);
            if(paymentIntent.status === 'succeeded'){
                console.log(paymentIntent.id);
                setTransactionId(paymentIntent.id)

                //save payment info to the database
                const payment ={
                    transactionId :paymentIntent.id,
                    email :user?.email ,
                    price :price,
                    date : new Date(),
                    cartId:bookings.map(item => item._id),
                    status : 'pending',

                }

              const res = await axiosSecure.post('/payments', payment)
              console.log('payment', res);
              if(res.data.paymentResult?.insertedId){
                toast.success('Payment Success')
                 navigate('/')
              }
            }
          }
    }
  return (
    <div>
        <form onSubmit={handleSubmit}>
        <div className="max-w-md mx-auto p-6 border rounded shadow-lg">
      <h2 className="text-2xl font-bold mb-4 text-center">Payment</h2>
      <div className="mb-4">
        <label className="block text-gray-700 text-sm font-bold mb-2">Total Amount</label>
        <input
          type="number"
          className="w-full px-3 py-2 border rounded focus:outline-none focus:shadow-outline"
          value={price}
          
        />
      </div>
      <div className="mb-4">
        <label className="block text-gray-700 text-sm font-bold mb-2">Card Details</label>
        <div className="border p-3 rounded">
          <CardElement />
        </div>
      </div>
      <button
      disabled={!stripe || !clientSecret }
        className="bg-blue-500 text-white px-4 py-2 rounded hover:bg-blue-700 focus:outline-none focus:shadow-outline"
         type='submit'
      >
        Pay Now
      </button>
      <p className='text-red-600 mt-4'>{error}</p>
      {
        transactionId && <p className='text-green-600 mt-4'>Your transaction id is  {transactionId}</p>
      }
    </div>
      
        </form>
    </div>
  )
}

export default PaymentForm