import React, { useEffect, useState } from 'react'
import useAuth from '../../hooks/useAuth'
import { useQuery } from '@tanstack/react-query'
import useAxiosSecure from '../../hooks/useAxiosSecure'
import { Pie } from 'react-chartjs-2'

const AdminHome = () => {
    // const {user}= useAuth()
    const axiosSecure = useAxiosSecure()
    const {data:adminHome={}}= useQuery({
        queryKey :['adminHome'],
        queryFn :async ()=>{
            const res =await axiosSecure.get('/admin-states')
            return res.data
        }
    })
 
   

      

  return (
    <div className=' min-h-screen bg-gray-100 '>
   <div className="p-20  ">
      <h1 className="text-3xl font-bold text-indigo-700 mb-6">Admin Dashboard</h1>

      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-4">
        <div className="bg-white p-6 rounded shadow-md">
          <h2 className="text-xl font-bold mb-4 text-blue-500 mr-5">Users : {adminHome.users}  </h2>
          {/* Display user data */}
        </div>

        <div className="bg-white p-6 rounded shadow-md">
          <h2 className="text-xl font-bold mb-4 text-green-500">Trainers :{adminHome.trainers}</h2>
          {/* Display trainer data */}
        </div>

        <div className="bg-white p-6 rounded shadow-md">
          <h2 className="text-xl font-bold mb-4 text-yellow-500">Orders :{adminHome.orders}</h2>
          {/* Display order data */}
        </div>

        <div className="bg-white p-6 rounded shadow-md">
          <h2 className="text-xl font-bold mb-4 text-red-500">Revenue :{adminHome.revenue}</h2>
          {/* Display revenue data */}
        </div>
      </div>
    </div>


 
    </div>
  )
}

export default AdminHome