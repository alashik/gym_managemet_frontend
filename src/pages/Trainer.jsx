import React, { useEffect, useState } from "react";
import TrainerCard from "./Trainer/TrainerCard";
import { Helmet } from "react-helmet-async";

const Trainer = () => {
  const [trainers, setTrainers] = useState([]);
  useEffect(() => {
    fetch("https://fitness-backend-gamma.vercel.app/trainers")
      .then((res) => res.json())
      .then((data) => setTrainers(data));
  }, []);

 

  // console.log(trainers);
  return (
    <div>
      <Helmet>
        <title>Fitness || Trainer</title>
        <link rel="canonical" href="https://www.tacobell.com/" />
      </Helmet>
      <div className=" lg:w-[1200px] mx-auto grid grid-cols-1 lg:grid-cols-4 gap-5 px-5">
        {trainers.map((trainer) => (
          <TrainerCard trainer={trainer} key={trainer._id} />
        ))}
      </div>
    </div>
  );
};

export default Trainer;
