import React from 'react'
import useAxiosSecure from '../../hooks/useAxiosSecure'
import { useQuery } from '@tanstack/react-query'
import axios from 'axios'
import useAxiosPublic from '../../hooks/useAxiosPublic'
import toast from 'react-hot-toast'
import { data } from 'autoprefixer'

const ApplyTRainer = () => {
    const  axiosSecure= useAxiosSecure()
    const  axiosPublic= useAxiosPublic()
    const {data:applyTrainer=[] ,refetch }= useQuery({
        queryKey: ['applyTrainer'],
        queryFn :async()=>{
            const res = await axiosSecure.get('/applyTrainer')
            return res.data
        }
    })
  

    const handleAddTrainer=async(apply)=>{
        const {age ,available_slots,email ,image,name,skill } =apply
        console.log(apply);
        const newApply={
          age ,available_slots,email ,image,name,skill
        }
         
        console.log(newApply);
           try {
            
            const res = await axiosPublic.post('/trainers' ,newApply)
            if(res.data.insertedId){
                toast.success('Successfully  add  Trainers ..!')
            }
            const  del = await axiosPublic.delete(`/applyTrainer/${apply._id}`)
             if(del.data.deletedCount > 0 ){
                toast.success('remove trainer')
                refetch ();

            }
            console.log(res);
           } catch (error) {
            toast.error(error.message)
           }
    }

    const handleTrainerDelete = async(id)=>{
        try {
            const res = await axiosPublic.delete(`/applyTrainer/${id}`)
            if(res.data.deletedCount > 0){
                toast.success('Deleted Successfully')
                refetch()
            }
        } catch (error) {
            toast.error(error.message)
        }
    }


  return (
    <div>
            <div>
        <div className="flex justify-between">
        <h2 className="text-4xl">All users </h2>
        <h2 className="text-4xl">Total Users: {applyTrainer.length} </h2>
      </div>

      <div className="overflow-x-auto">
        <table className="table">
          {/* head */}
          <thead>
            <tr className='text-center'>
              <th>#</th>
              <th>Name</th>
              <th>Email</th>
              <th>Skills</th>
              <th>Age</th>
              <th>Day</th>
              <th>Time</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {applyTrainer.map((applyTr, i) => (
              <tr key={applyTr._id} className=''>
                <th> {i + 1}</th>
                <td>{applyTr.name}</td>
                <td>{applyTr.email}</td>
                <td className='w-[200px]'>{applyTr.skill}</td>
                <td>{applyTr.age}</td>
                <td>{applyTr.available_slots[0].day}</td>
                <td>{applyTr.available_slots[0].start_time}</td>
               
                <td className='flex mt-3'>
                <button className="btn btn-sm btn-success text-white mr-3" onClick={()=>handleAddTrainer(applyTr)} >Accept</button>
                <button className="btn btn-sm  btn-error  text-white  "  onClick={()=>handleTrainerDelete(applyTr._id)}>Reject</button>

                </td>
                 
                
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
    </div>
  )
}

export default ApplyTRainer