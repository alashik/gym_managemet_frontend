import React from 'react'
import useAxiosSecure from '../../hooks/useAxiosSecure'
import { useQuery } from '@tanstack/react-query'

const AllTrainer = () => {
    const  axiosSecure= useAxiosSecure()
    const {data:trainers=[] }= useQuery({
        queryKey: ['trainer'],
        queryFn :async()=>{
            const res = await axiosSecure.get('/trainers')
            return res.data
        }
    })

    console.log(trainers);
  return (
    <div>
         <div>
        <div className="flex justify-between">
        <h2 className="text-4xl">All  Trainers </h2>
        <h2 className="text-4xl">Total Trainers: {trainers.length} </h2>
      </div>

      <div className="overflow-x-auto">
        <table className="table">
          {/* head */}
          <thead>
            <tr>
            <th>#</th>
            <th>Name</th>
              <th>Email</th>
              <th>Skills</th>
              <th>Age</th>
              <th>Day</th>
              <th>Time</th>
            </tr>
          </thead>
          <tbody>
            {trainers.map((trainer, i) => (
              <tr key={trainer._id}>
              <th> {i + 1}</th>
                <td>{trainer.name}</td>
                <td>{trainer.email}</td>
                <td className='w-[200px]'>{trainer.skill}</td>
                <td>{trainer.age}</td>
                <td>{trainer.available_slots[0].day}</td>
                <td>{trainer.available_slots[0].start_time}</td>
                 
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
    </div>
  )
}

export default AllTrainer