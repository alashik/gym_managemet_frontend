import React from 'react'
import AboutSections from './About/AboutSections'

const About = () => {
  return (
    <div>
        <AboutSections />
    </div>
  )
}

export default About