import axios from 'axios'
import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import useAxiosSecure from '../hooks/useAxiosSecure'
import toast from 'react-hot-toast'
import useAxiosPublic from '../hooks/useAxiosPublic'
import useAuth from '../hooks/useAuth'




const image_hosting_key =import.meta.env.VITE_IMAGE_HOSTING_KEY
const image_hosting_api = `https://api.imgbb.com/1/upload?key=${image_hosting_key}`
const BeaTrainer = () => {
  const axiosPublic= useAxiosPublic()
  const {user}= useAuth()
   const {register , handleSubmit ,reset } = useForm()
  const axiosSecure = useAxiosSecure()


   const onSubmit =async(data)=> {

    // IMAGE UPLOAD 
    const imageFile ={image :data.image[0]}
    const res =await axios.post(image_hosting_api ,imageFile ,{
        headers :{
            "content-type" :"multipart/form-data"
        }
    })
    if(res.data.success){
        // now send the data to the server with image url 
        const trainer ={
            name : data.name,
            email :user.email,
            age : data.age,
            image : res.data.data.display_url,
            skill :data.skills,
            available_slots : [
                {
                    day : data.availableDay,
                    start_time : data.time
                    
                }
            ],

            
        }
        const trainers = await axiosPublic.post('/applyTrainer' , trainer)

        console.log(trainers.data);
        if(trainers.data.insertedId){
          toast.success('Request Successfully  !')
        }
    }
    console.log(data);
 reset()
   }
    

  return (
  

    <div className="container mx-auto max-w-md">
    <h1 className="text-3xl font-bold mb-4">Gym Trainer Registration</h1>

    <form onSubmit={handleSubmit(onSubmit)}  className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4" >

      {/* Name Input */}
      <div className="mb-4">
        <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="name">
          Name
        </label>
        <input
        {...register('name' ,{required:true})}
          name="name"
          className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          id="name" type="text" placeholder="Enter your name"  />
      </div>

      {/* Email Input */}
      <div className="mb-4">
        <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="email">
          Email
        </label>
        <input
        {...register('email' ,{required:true})}
          name="email"
          defaultValue={user?.email }
           
          className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          id="email" type="email" placeholder="Enter your email"   />
      </div>

      {/* Age Input */}
      <div className="mb-4">
        <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="age">
          Age
        </label>
      
        <input
          {...register('age' ,{required:true})}
          name="age"
          className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          id="age" type="number" placeholder="Enter your age"   />
      </div>

      {/* Image Upload Input */}
      <div className="mb-4">
        <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="image">
          Profile Image
        </label>
        <input
          {...register('image' ,{required:true})}
          name="image"
          className="appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          id="image" type="file"   />
        <p className="text-gray-600 text-xs mt-2">Upload a profile image</p>
      </div>

      {/* Skills Input */}
      <div className="mb-4">
        <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="skills">
          Skills
        </label>
        <textarea 
          {...register('skills' ,{required:true})}
          name="skills"
          className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          id="skills"
          placeholder="Enter your skills"
          
        ></textarea>
      </div>

      {/* Available Day Input */}
      <div className="mb-4">
        <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="availableDay">
          Available Day
        </label>
        <select 
          {...register('availableDay' ,{required:true})}
          name="availableDay"
          className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          id="availableDay"
      
        >
        
          <option value="monday">Monday</option>
          <option value="tuesday">Tuesday</option>
          <option value="wednesday">Wednesday</option>
          <option value="thursday">Thursday</option>
          <option value="friday">Friday</option>
        </select>
      </div>
      <div className="mb-4">
        <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="availableDay">
          Available Day
        </label>
        <select 
          {...register('time' ,{required:true})}
          name="time"
          className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          id="availableDay"
      
        >
              
          <option value="7:00 am - 8:00 am">7:00 am - 8:00  am</option>
          <option value="8:00 am - 9:00 am">8:00 am - 9:00 am</option>
          <option value="9:00 am - 10:00 am">9:00 am - 10:00 am</option>
          <option value="6:00 pm - 7:00 pm">6:00 pm - 7:00 pm</option>
          <option value="7-00 pm -8:00 pm">7-00 pm -8:00 pm</option>
          <option value="8:00 pm - 9:00 pm">8:00 pm - 9:00 pm</option>
      
        </select>
      </div>

     

      {/* Submit Button */}
      <div className="flex items-center justify-between">
        <button
          className="bg-blue-500 text-white px-4 py-2 rounded-full focus:outline-none focus:shadow-outline"
          type="submit"
        >
          Register
        </button>
      </div>

    </form>
  </div>



  )
}

export default BeaTrainer