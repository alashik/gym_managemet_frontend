import React, { useEffect, useState } from "react";
import img1 from "../assets/features/scott-webb-U5kQvbQWoG0-unsplash.jpg";
import img2 from "../assets/features/graham-mansfield-y7ywDXWJ-JU-unsplash.jpg";
import img3 from "../assets/features/valery-sysoev-LDAirERNzew-unsplash - Copy.jpg";
import useAxiosPublic from "../hooks/useAxiosPublic";
import { useQuery } from "@tanstack/react-query";
import { AiFillDelete } from "react-icons/ai";
import useAuth from "./../hooks/useAuth";
import useBooking from "../hooks/useBooking";
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet-async";
const PlansAndBooking = () => {
  const { user,priceArray ,priceCart,calculateSum } = useAuth();
  // const [priceArray, setPriceArray] = useState([]);

  // const [bookings ,setBookings]= useState([])

  const [bookings] = useBooking();

  const handlePackage = (e) => {
    e.preventDefault();
    priceCart(e.target.package.value)
    // const price = parseInt(e.target.package.value);
    // setPriceArray([...priceArray, price]);
  };
  // const calculateSum = () => {
  //   return priceArray.reduce((sum, price) => sum + price, 0);
  // };

  console.log('sum',priceArray);
  console.log('sum',calculateSum());

  return (
    <div>
        <Helmet>
        <title>Fitness ||  Booking</title>
        <link rel="canonical" href="https://www.tacobell.com/" />
      </Helmet>
      <div className="container mx-auto lg:flex justify-center items-center mt-10 px-4 ">
        {/* Silver Package Card */}
        <div className="max-w-sm mx-4 bg-white border border-gray-300 shadow-sm rounded-md overflow-hidden mb-5">
          <div className="bg-gray-200 py-4 text-center">
            <h2 className="text-xl font-bold">Silver Package</h2>
          </div>
          <img
            src={img1}
            alt="Silver Package"
            className="w-full h-32 object-cover"
          />
          <div className="p-4">
            <ul className="list-disc list-inside">
              <li>Access to gym equipment</li>
              <li>Basic training plan</li>
            </ul>
            <p className="mt-4 text-lg font-bold">$50/month</p>
            <button className="mt-4 bg-blue-500 text-white px-6 py-2 rounded-full">
              Book Now
            </button>
          </div>
        </div>
        {/* Gold Package Card */}
        <div className="max-w-sm mx-4 bg-white border border-gray-300 shadow-sm rounded-md overflow-hidden">
          <div className="bg-yellow-300 py-4 text-center">
            <h2 className="text-xl font-bold">Gold Package</h2>
          </div>
          <img
            src={img2}
            alt="Gold Package"
            className="w-full h-32 object-cover"
          />
          <div className="p-4">
            <ul className="list-disc list-inside">
              <li>Access to gym equipment</li>
              <li>Personalized training plan</li>
              <li>1 personal training session/week</li>
            </ul>
            <p className="mt-4 text-lg font-bold">$80/month</p>
            <button className="mt-4 bg-blue-500 text-white px-6 py-2 rounded-full">
              Book Now
            </button>
          </div>
        </div>
        {/* Diamond Package Card */}
        <div className="max-w-sm mx-4 bg-white border border-gray-300 shadow-sm rounded-md overflow-hidden">
          <div className="bg-yellow-500 py-4 text-center">
            <h2 className="text-xl font-bold">Diamond Package</h2>
          </div>
          <img
            src={img3}
            alt="Diamond Package"
            className="w-full h-32 object-cover"
          />
          <div className="p-4">
            <ul className="list-disc list-inside">
              <li>Unlimited gym access</li>
              <li>Personalized training plan</li>
              <li>2 personal training sessions/week</li>
              <li>Nutritional guidance</li>
            </ul>
            <p className="mt-4 text-lg font-bold">$120/month</p>
            <button className="mt-4 bg-blue-500 text-white px-6 py-2 rounded-full">
              Book Now
            </button>
          </div>
        </div>
      </div>

      <div className="lg:w-[1000px] lg:mx-auto mt-10 mb-10">
        <table className="table">
          {/* head */}
          <thead>
            <tr className="text-center">
              <th> #</th>
              <th>Trainer Image</th>
              <th>Trainer Name</th>

              <th>Day</th>
              <th>Available Slots</th>
              <th>Package</th>
            </tr>
          </thead>
          <tbody>
            {/* row 1 */}

            {bookings?.map((item, i) => (
              <tr key={item._id}>
                <th>{i + 1}</th>
                <td>
                  <div className="flex items-center gap-3">
                    <div className="avatar">
                      <div className="mask mask-squircle w-16 h-16">
                        <img src={item.image} />
                      </div>
                    </div>
                  </div>
                </td>
                <td>{item.name}</td>

                <td>$ {item.available_slots[0].day}</td>
                <td>$ {item.available_slots[0].start_time}</td>
                <td>
                  <form action="" onSubmit={handlePackage}>
                    <div className="flex gap-3 items-center">
                      <select
                        name="package"
                        className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                        id="package"
                      >
                        <option disabled selected>
                          Select a package
                        </option>
                        <option value="50">Silver - $50</option>
                        <option value="80">Gold - $80</option>
                        <option value="120">Diamond - $120 </option>
                      </select>
                      <button
                        type="submit"
                        className="btn btn-outline  btn-sm  "
                      >
                        Confirm Package
                      </button>
                    </div>
                  </form>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      <div className="divider"> </div>

      <div className="bg-gray-100 p-10 rounded-md max-w-lg mx-auto mb-10">
        <div className="flex justify-between items-center mb-2">
          <span className="font-semibold">Package Price: </span>
          <span className="text-orange-600 font-extrabold">
            $ {calculateSum()}
          </span>

          {bookings.length ? (
            <Link to="/dashboard/payment">
              <button
                // onClick={onPayment}
                className="mt-4 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
              >
                
                Pay Now
              </button>
            </Link>
          ) : (
            <button
              disabled
              className="mt-4 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
            >
             
              Pay Now
            </button>
          )}
        </div>
      </div>
      <div></div>
    </div>
  );
};

export default PlansAndBooking;
