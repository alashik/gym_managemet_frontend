import React from 'react'
import { useEffect } from 'react';
import { Link } from 'react-router-dom';
import useAxiosPublic from '../../hooks/useAxiosPublic';
import toast from 'react-hot-toast';
import useAuth from '../../hooks/useAuth';
import useBooking from '../../hooks/useBooking';

const Avaliable = ({slots ,loadTrainer}) => {
  console.log(loadTrainer);
  const {user}= useAuth()
  const [, refetch]=useBooking()
  const {name,age,image,skill,available_slots ,email}= loadTrainer
  const axiosPublic = useAxiosPublic()
  const  newTrainer = {
    name,
    age,
    email:user?.email,
    image,
    skill,
    available_slots,
     trainer_email :email
  }
    // console.log('thisis' ,slots);

    const handleBookings =async()=>{
      
        
       try {
     
        const res = await axiosPublic.post(`/bookings`,newTrainer)
       console.log(res.data.insertedId);
        if(res.data.insertedId ){
          toast.success(' Booked Successfully!') 
          refetch()
        }else{

        }
       } catch (error) {
        toast.error('Already added')
       }
    }

   
  return (
    <div>
  <div className="w-1200 mx-auto">
  <table className="min-w-full bg-white border border-gray-300 shadow-sm rounded-md overflow-hidden">
    <thead className="bg-gray-200">
      <tr>
        <th className="py-2 px-4 border-b">Day</th>
        <th className="py-2 px-4 border-b"> Time</th>
      
      </tr>
    </thead>
    <tbody>
    
      {
        slots.map(({day,start_time})=>(
          
            
    
        <tr>
          <td class="py-2 px-4 border-b text-center">{day}</td>
          <td class="py-2 px-4 border-b text-center">{start_time}</td>
       
        </tr>
      
         
        ))
      }
    <div className='flex justify-center'>
        
 
    </div>
      
      
    </tbody>
  </table>

      <div >
     <Link  onClick={handleBookings}>
     <button className="btn btn-success w-full mt-5">Cart to Booked</button>
     </Link>
      </div>
</div>


    </div>
  )
}

export default Avaliable