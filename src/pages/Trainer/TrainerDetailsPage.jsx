import React, { useEffect } from "react";
import { FaFacebook, FaInstagram, FaLinkedin } from "react-icons/fa";
import { useLoaderData } from "react-router-dom";
import Avaliable from "./Avaliable";
import { Helmet } from "react-helmet-async";
 
const TrainerDetailsPage = () => {
  const loadTrainer = useLoaderData();
  const { name, image, experience_years ,available_slots
  } = loadTrainer;

  console.log(image);
 

  // console.log(loadTrainer);

  return (
    <div>
        <Helmet>
        <title>Details Page Title</title>
        <meta property="og:title" content="Details Page Title" />
        <meta property="og:description" content="Description of the details page." />
        <meta property="og:image" content={image} />
        <meta property="og:url" content={window.location.href} />
      </Helmet>

    <div className="lg:w-[1200px] mx-auto">
      <div className="lg:flex gap-10  my-10">
        <div className="lg:w-2/3">
         <div>
         <img className="w-full h-[400px]  " src={image} alt="" />
         </div>
          
        </div>
        <div className="lg:w-1/3 px-5 mt-10">
          <h3 className="text-3xl font-medium text-[#87C232]">
            LIFE IS MORE FUN
          </h3>
          <h1 className="text-4xl font-bold italic mt-2">WHEN YOU FEEL FIT</h1>

          <div className="divider"></div>

          <div>
            <h2 className=" text-2xl font-bold italic text-[#86c232e8] ">
              {name}
            </h2>
            <p className="text-lg mt-2">
            
              Experience years 
              <span className="text-xl font-bold    text-orange-400 ml-2">
                
                {experience_years} 
              </span>
            </p>

            <div className=" flex gap-3 mt-5  text-2xl   ">
              <a href="#" className="  hover:underline">
                <span>
                  <FaFacebook />
                </span>
              </a>
              <a href="#" className="  hover:underline">
                <span>
                  <FaLinkedin />
                </span>
              </a>
              <a href="#" className=" hover:underline">
                <span>
                  <FaInstagram />
                </span>
              </a>
            </div>

            <div className="mt-8 ">
              <h1 className="text-2xl text-center font-bold  underline">Available Slots</h1>
                <Avaliable  slots ={available_slots} loadTrainer={loadTrainer}/>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  );
};

export default TrainerDetailsPage;
