import React from "react";
import { FaFacebook, FaInstagram, FaLinkedin } from "react-icons/fa";
import { Link } from "react-router-dom";
const TrainerCard = ({trainer}) => {
//  console.log(trainer);




 
  return (
    <div className="mt-10">
      <div className="card shadow-xl  ">
        
          <img
            src={trainer.image}
            alt="Shoes"
            className="h-[250px]"
          />
        
        <div className="text-center my-5  ">
          <h2 className=" text-2xl font-bold italic text-[#86c232e8] ">{trainer.name}</h2>
          <p className="text-lg">Experience years <span className="text-xl font-bold text-orange-400 ml-2"> {trainer.experience_years}</span>  </p>
         
        </div>
        <div className=" flex gap-3 justify-center  text-2xl   ">
        <a href="#" className="  hover:underline"><span><FaFacebook /></span></a>
        <a href="#" className="  hover:underline"><span><FaLinkedin /></span></a>
        <a href="#" className=" hover:underline"><span><FaInstagram /></span></a>

       
        </div> 
        <div className="text-end my-5 text-blue-600 font-bold underline  mr-9">
            <Link to={`/trainer/${trainer._id}`}>Know More</Link>
        </div>
      
      </div>
    </div>
  );
};

export default TrainerCard;
