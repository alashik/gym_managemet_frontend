import React from 'react'
import DaileClass from './DailyClass/DaileClass'
import { Helmet } from 'react-helmet-async';

const DailyClassShedule = () => {
    const days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
    const timeSlots = ['Slot 1', 'Slot 2', 'Slot 3', 'Slot 4', 'Slot 5', 'Slot 6'];
    
    const classes = {
      Monday: {
        'Slot 1': '  (7:00 AM - 8:00 AM)',
        'Slot 2': '  (8:00 AM - 9:00 AM)',
        'Slot 4': '  (9:00 AM - 10:00 AM)',
        'Slot 5': '  (6:00 PM - 7:00 PM)',
        'Slot 6': '  (7:00 PM - 8:00 PM)',
        'Slot 3': '  (8:00 PM - 9:00 PM)',
        // Add classes for other slots
      },
      Tuesday: {
        // Add classes for each slot
        'Slot 1': '  (7:00 AM - 8:00 AM)',
        'Slot 2': '  (8:00 AM - 9:00 AM)',
        'Slot 4': '  (9:00 AM - 10:00 AM)',
        'Slot 5': '  (6:00 PM - 7:00 PM)',
        'Slot 6': '  (7:00 PM - 8:00 PM)',
        'Slot 3': '  (8:00 PM - 9:00 PM)',
      },
      Wednesday: {
        // Add classes for each slot
        'Slot 1': '  (7:00 AM - 8:00 AM)',
        'Slot 2': '  (8:00 AM - 9:00 AM)',
        'Slot 4': '  (9:00 AM - 10:00 AM)',
        'Slot 5': '  (6:00 PM - 7:00 PM)',
        'Slot 6': '  (7:00 PM - 8:00 PM)',
        'Slot 3': '  (8:00 PM - 9:00 PM)',
      },
      Thursday: {
        // Add classes for each slot
        'Slot 1': '  (7:00 AM - 8:00 AM)',
        'Slot 2': '  (8:00 AM - 9:00 AM)',
        'Slot 4': '  (9:00 AM - 10:00 AM)',
        'Slot 5': '  (6:00 PM - 7:00 PM)',
        'Slot 6': '  (7:00 PM - 8:00 PM)',
        'Slot 3': '  (8:00 PM - 9:00 PM)',
      },
      Friday: {
        // Add classes for each slot
        'Slot 1': '  (7:00 AM - 8:00 AM)',
        'Slot 2': '  (8:00 AM - 9:00 AM)',
        'Slot 4': '  (9:00 AM - 10:00 AM)',
        'Slot 5': '  (6:00 PM - 7:00 PM)',
        'Slot 6': '  (7:00 PM - 8:00 PM)',
        'Slot 3': '  (8:00 PM - 9:00 PM)',
      },
    };
    
  return (
    <div>

<Helmet>
        <title>Fitness || Class Schedule</title>
        <link rel="canonical" href="https://www.tacobell.com/" />
      </Helmet>
    <DaileClass days={days} timeSlots={timeSlots} classes={classes}/>

    </div>
  )
}

export default DailyClassShedule