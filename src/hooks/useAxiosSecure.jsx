import axios from 'axios'
import { useNavigate } from 'react-router-dom'
import useAuth from './useAuth';
 


const axiosSecure = axios.create({
    baseURL:'https://fitness-backend-gamma.vercel.app/'
  })
const useAxiosSecure = () => {
  const navigate = useNavigate()
  const { logOut}= useAuth()
  axiosSecure.interceptors.request.use(function(config){
    const token = localStorage.getItem('access-token')
    // console.log('hit intersectors');
    config.headers.authorization =`Bearer ${token}`
    return config
  },function (error) {
    // Do something with request error
    return Promise.reject(error);
  })

  // axios intersectors for 401 and 403 
  axiosSecure.interceptors.response.use(function (response) {
    return response;
  }, async function (error) {
    const status =error.response.status 
    if(status === 401 || status === 403){
      await logOut()
        navigate('/login')
    }
      // console.log('interseptor error' , error);
    return Promise.reject(error);
  }); 
  // axios for 401 and 403 

  return  axiosSecure
}

export default useAxiosSecure