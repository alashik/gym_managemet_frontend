import React from 'react'
import useAxiosPublic from './useAxiosPublic'
import { useQuery } from '@tanstack/react-query'
import useAuth from './useAuth'

const useBooking = () => {
    const {user}= useAuth()
    const axiosPublic=useAxiosPublic()

    const { data :bookings=[] ,refetch } =  useQuery({
        queryKey:['booking' , user?.email],
        queryFn: async ()=>{
         const res = await axiosPublic.get(`/bookings?email=${user.email}`)
         return res.data
        }
    })

    return [bookings ,refetch]
}

export default useBooking