import React, { createContext, useEffect, useState } from "react";
 
import auth from "../config/firebase.config";
import { GoogleAuthProvider, createUserWithEmailAndPassword, onAuthStateChanged, signInWithEmailAndPassword, signInWithPopup, signOut, updateProfile } from "firebase/auth";
import useAxiosPublic from './../hooks/useAxiosPublic';
export const AuthContext = createContext(null);

const AuthProvider = ({ children }) => {
  const [isLoading, setIsLoading] = useState(true);
  const [priceArray, setPriceArray] = useState([]);
  const [user, setUser] = useState(null);
  const axiosPublic= useAxiosPublic()
  console.log(user);
  const googleProvider = new GoogleAuthProvider();
  const createUser = ( email ,password)=>{
    setIsLoading(true)
    return createUserWithEmailAndPassword(auth,email,password)
  }
                                                                   
  const loginUser =(email, password)=>{
      setIsLoading(true)
      return signInWithEmailAndPassword(auth ,email, password)
  }
  const googleLogin =()=>{
      setIsLoading(true)
      return signInWithPopup(auth ,googleProvider)
  }

  const logOut = ()=>{
    setIsLoading(true)
    return signOut(auth)
  }
  const updateUser =(name , photo)=>{
    setIsLoading(true)
    return updateProfile(auth.currentUser, {
      displayName: name, photoURL:photo
    })
  }

  const priceCart= async(value)=>{
    const price = parseInt(value);
    setPriceArray([...priceArray, price]);
  }

  const calculateSum = () => {
    return priceArray.reduce((sum, price) => sum + price, 0);
  };
 
  useEffect(()=>{
      const subscribtion = onAuthStateChanged(auth,(currentUser)=>{
        setUser(currentUser)
        if(currentUser){
            // get token and store client
            const userInfo = {email :currentUser.email}
            axiosPublic.post('/jwt',userInfo)
            .then(res =>{
              localStorage.setItem('access-token' ,res.data.token)
            })
            setIsLoading(false)
        }else{
            //remove token 
            localStorage.removeItem('access-token')
        }
        setIsLoading(false)
      })
      return ()=>{
        return subscribtion()
      }
  },[axiosPublic])

  // console.log(user);
  const values = {
    isLoading,
    user,
    createUser,
    loginUser,
    googleLogin,
    logOut,
    updateUser,
    priceCart,
    priceArray,
    calculateSum
  }
   

  return <AuthContext.Provider value={values}>{children}</AuthContext.Provider>;
};

export default AuthProvider;
