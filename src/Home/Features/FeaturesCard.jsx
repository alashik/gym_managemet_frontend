import React from "react";

const FeaturesCard = ({img ,subTitle,title}) => {
  return (
    <div>
      <div className=" custom-box-shadow mb-5  rounded h-[450px] px-5">
       
       <img src={img} alt="" className="h-[200px]  w-full rounded relative cursor-pointer"  />
      
        <div className="lg:px-8 lg:py-8">
         <h3 className="text-[#87C232] text-lg font-bold">{subTitle}</h3>
         <div className="divider"></div>
         <h1 className=" text-xl font-extrabold mt-2">{title}</h1>
        
        </div>
      </div>
    </div>
  );
};

export default FeaturesCard;
