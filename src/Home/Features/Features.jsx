import React from "react";
import FeaturesCard from "./FeaturesCard";


import featureImg1 from '../../assets/features/alonso-reyes-0HlI76m4jxU-unsplash.jpg'
import featureImg2 from '../../assets/features/anastase-maragos-9dzWZQWZMdE-unsplash - Copy.jpg'
import featureImg3 from '../../assets/features/anastase-maragos-HyvE5SiKMUs-unsplash.jpg'
import featureImg4 from '../../assets/features/graham-mansfield-y7ywDXWJ-JU-unsplash.jpg'
import featureImg5 from '../../assets/features/scott-webb-U5kQvbQWoG0-unsplash.jpg'
import featureImg6 from '../../assets/features/total-shape-Drq07X98BqM-unsplash.jpg'
import featureImg7 from '../../assets/features/valery-sysoev-LDAirERNzew-unsplash - Copy.jpg'
 
const Features = () => {
  return (
    <div className="lg:w-[1200px] lg:mx-auto my-20">
      <div className="lg:flex justify-between  px-5">
      <div>
        <h3 className=" mb-6 text-4xl font-medium text-[#87C232]">GROW ON PURPOSE</h3>
        <h1 className=" text-5xl font-bold">TIPS, TRICKS & NEWS</h1>
       </div>
      <div className="mt-12">
      <button className="px-6 p-4 text-lg bg-[#6d9b2e] text-white font-bold rounded uppercase italic">See all classes</button>
      </div>
      </div>
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-5 mt-20">
        <FeaturesCard img={featureImg1} subTitle='LIFESTYLE , NUTRITION' title='WHY FITTLIFE IS THE BEST THEME FOR YOUR BUSINESS' />
        <FeaturesCard img={featureImg2}  subTitle='HEALTHY,NUTRITION' title='HOW TO HAVE A SUCCESSFUL PERSONAL TRAINING '  />
        <FeaturesCard img={featureImg3}  subTitle='FITNESS,MOTIVATION' title='THIS IS HOW YOU CHOOSE THE RIGHT NUTRITION COURSE'  />
        <FeaturesCard img={featureImg4}  subTitle='WEIGHT LOSS,WORKOUT' title='GYM MACHINE WORKOUT ROUTINE FOR BEGINNERS'  />
        <FeaturesCard img={featureImg5}  subTitle='LIFESTYLE , NUTRITION' title='WHY FITTLIFE IS THE BEST THEME FOR YOUR BUSINESS'  />
        <FeaturesCard img={featureImg6} subTitle='FITNESS,MOTIVATION' title='THIS IS HOW YOU CHOOSE THE RIGHT NUTRITION COURSE'   />
        <FeaturesCard img={featureImg7} subTitle='WEIGHT LOSS,WORKOUT' title='GYM MACHINE WORKOUT ROUTINE FOR BEGINNERS'   />
      </div>
    </div>
  );
};

export default Features;
