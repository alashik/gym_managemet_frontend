import React from "react";
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';



import { Pagination, Navigation } from 'swiper/modules';
import { Swiper, SwiperSlide } from 'swiper/react';

import img1 from  '../../assets/banner/anastase-maragos-9dzWZQWZMdE-unsplash.jpg'
import img2 from  '../../assets/banner/daniel-apodaca-WdoQio6HPVA-unsplash.jpg'
// import img3 from  '../../assets/banner/jonathan-borba-zfPOelmDc-M-unsplash.jpg'
import img4 from  '../../assets/banner/sven-mieke-jO6vBWX9h9Y-unsplash.jpg'
import img5 from  '../../assets/banner/valery-sysoev-LDAirERNzew-unsplash.jpg'
import BannerComponents from "./BannerComponents";
const Banner = () => {
  return (
    <div>
        <Swiper
        
        navigation={true}
        modules={[Pagination, Navigation]}
        className="mySwiper"
      >
        <SwiperSlide>
          <BannerComponents img={img1} />
        </SwiperSlide>
        <SwiperSlide>
          <BannerComponents img={img2} />
        </SwiperSlide>
        
        <SwiperSlide>
          <BannerComponents img={img4} />
        </SwiperSlide>
        <SwiperSlide>
          <BannerComponents img={img5} />
        </SwiperSlide>
       
    
      </Swiper>
    </div>
  );
};

export default Banner;
