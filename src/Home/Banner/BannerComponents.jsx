import React from "react";

const BannerComponents = ({ img }) => {
  return (
    <div>
      <div
        className="hero min-h-[550px]"
        style={{ backgroundImage: `url(${img})` }}
      >
        <div className="bg-opacity-80"></div>
        <div className="hero-content text-center text-neutral-content">
          <div className="max-w-3xl">
            <h4 className="mb-6 text-4xl lg:text-5xl font-medium text-[#87C232]">STAY FIT, STAY HEALTHY</h4>
            <h1 className="mb-6 text-4xl lg:text-5xl  font-bold">GET BACK IN SHAPE NOW!</h1>
            <p className="mb-6">Train in an award winning gym that has been featured in multiple magazines for offering excellent fitness classes  </p>
           <div className="flex gap-3 lg:gap-10 justify-center items-center">
           <button className=" px-3 lg:px-6 py-4 text-lg bg-[#6d9b2e] font-bold rounded uppercase italic">See all classes</button>
           <button className="px-3 lg:px-6 py-4 text-lg  border font-bold rounded uppercase italic hover:bg-white hover:text-black  hover:ease-in duration-500 ">Join Membership</button>
           {/* <button className="btn  bg-[#6d9b2e] text-lg text-white  ">See all classes</button> */}
          
           </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BannerComponents;
