import React from "react";
import aboutImg from "../../assets/features/alonso-reyes-0HlI76m4jxU-unsplash.jpg";
import aboutImg2 from "../../assets/features/anastase-maragos-HyvE5SiKMUs-unsplash.jpg";
import { FaUser  } from "react-icons/fa";
import { FaMessage } from "react-icons/fa6";
import { BiSolidMemoryCard } from "react-icons/bi";
import 'react-accessible-accordion/dist/fancy-example.css';
import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemButton,
    AccordionItemPanel,
} from 'react-accessible-accordion';
const BlogSection = () => {
  return (
    <div className="lg:w-[1200px] lg:mx-auto px-5">
      <div className="text-center mt-16">
        <h1 className="text-5xl font-medium italic text-[#9bcf51]">OUR BLOG</h1>
        <h3 className="text-2xl">INSIGHTS, NEWS & TRENDS</h3>
      </div>

      <div class=" lg:flex my-10 gap-10">
        <div class="lg:w-2/3">
           <div>
           <img src={aboutImg} alt="" className="h-[300px] w-full rounded" />
            <div>
                <div className="lg:flex justify-between px-5 mt-5">
                   
                    <p className="flex  items-center gap-3 font-bold italic "> <span className="text-[#91c943]"> <FaUser /></span> QREATIVETHEMES</p>
                    <p className="flex  items-center gap-3 font-bold italic"> <span className="text-[#91c943]"><BiSolidMemoryCard /></span>  LIFESTYLE, NUTRITION</p>
                    <p className="flex  items-center gap-3 font-bold italic"> <span className="text-[#91c943]">  <FaMessage /> </span> 0 COMMENTS</p>
                   
                </div>

                <div className="divider"></div> 

            <h1 className="text-3xl font-extrabold italic mt-6  ">WHY FITTLIFE IS THE BEST THEME FOR YOUR BUSINESS</h1>
            <p className="text-gray-500 leading-loose mt-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Arcu dui vivamus arcu felis. Amet risus nullam eget felis eget nunc lobortis mattis. Non sodales neque sodales ut etiam sit amet. Ac auctor augue mauris augue neque gravida. Pulvinar neque laoreet suspendisse interdum consectetur libero.…</p>
            
            </div>
           </div>
           <div>
           <img src={aboutImg2} alt="" className="h-[300px] w-full rounded" />
            <div>
                <div className="lg:flex justify-between px-5 mt-5">
                   
                    <p className="flex  items-center gap-3 font-bold italic "> <span className="text-[#91c943]"> <FaUser /></span> QREATIVETHEMES</p>
                    <p className="flex  items-center gap-3 font-bold italic"> <span className="text-[#91c943]"><BiSolidMemoryCard /></span>  LIFESTYLE, NUTRITION</p>
                    <p className="flex  items-center gap-3 font-bold italic"> <span className="text-[#91c943]">  <FaMessage /> </span> 0 COMMENTS</p>
                   
                </div>

                <div className="divider"></div> 

            <h1 className="text-3xl font-extrabold italic mt-6  ">WHY FITTLIFE IS THE BEST THEME FOR YOUR BUSINESS</h1>
            <p className="text-gray-500 leading-loose mt-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Arcu dui vivamus arcu felis. Amet risus nullam eget felis eget nunc lobortis mattis. Non sodales neque sodales ut etiam sit amet. Ac auctor augue mauris augue neque gravida. Pulvinar neque laoreet suspendisse interdum consectetur libero.…</p>
            
            </div>
           </div>
        </div>
        <div class="lg:w-1/3">
            <div>
                <h1 className="text-4xl font-bold italic text-[#a1d459] mb-5 ">About Us</h1>
                <p className="leading-loose text-gray-500 mb-5">Fittlife is a premium WordPress theme built for the Fitness & Gym industry but is suitable for all that provide Sports & Nutrition services. Novice or experienced, this theme will suit your needs to create a website that is build to last.</p>
                <button className="px-6 py-4 text-lg bg-[#6d9b2e] font-bold rounded uppercase italic text-white">See all classes</button>
            </div>

            <div className="mt-10">
                <h3 className="text-xl font-bold italic mt-10">RECENT BLOG POSTS</h3>


                <Accordion>
            <AccordionItem>
                <AccordionItemHeading>
                    <AccordionItemButton>
                    Why Fittlife is The Best Theme For Your Business
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                    <p>
                        Exercitation in fugiat est ut ad ea cupidatat ut in
                        cupidatat occaecat ut occaecat consequat est minim minim
                        esse tempor laborum consequat esse adipisicing eu
                        reprehenderit enim.
                    </p>
                </AccordionItemPanel>
            </AccordionItem>
            <AccordionItem>
                <AccordionItemHeading>
                    <AccordionItemButton>
                    How to Have a Successful Personal Training Business
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                    <p>
                        In ad velit in ex nostrud dolore cupidatat consectetur
                        ea in ut nostrud velit in irure cillum tempor laboris
                        sed adipisicing eu esse duis nulla non.
                    </p>
                </AccordionItemPanel>
            </AccordionItem>
            <AccordionItem>
                <AccordionItemHeading>
                    <AccordionItemButton>
                    This is How You Choose the Right Nutrition Course
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                    <p>
                        In ad velit in ex nostrud dolore cupidatat consectetur
                        ea in ut nostrud velit in irure cillum tempor laboris
                        sed adipisicing eu esse duis nulla non.
                    </p>
                </AccordionItemPanel>
            </AccordionItem>
            <AccordionItem>
                <AccordionItemHeading>
                    <AccordionItemButton>
                    This is How You Choose the Right Nutrition Course
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                    <p>
                        In ad velit in ex nostrud dolore cupidatat consectetur
                        ea in ut nostrud velit in irure cillum tempor laboris
                        sed adipisicing eu esse duis nulla non.
                    </p>
                </AccordionItemPanel>
            </AccordionItem>
            
        </Accordion>
            </div>
        </div>
      </div>
    </div>
  );
};

export default BlogSection;
