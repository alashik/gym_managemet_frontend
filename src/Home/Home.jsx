import React from 'react'
import Banner from './Banner/Banner'
import Features from './Features/Features'
import About from '../pages/About'
import NewsLetterSection from './NewsLetter/NewsLetterSection'
import BlogSection from './Blog/BlogSection'
import Trainer from '../pages/Trainer'
import { Helmet } from 'react-helmet-async'

const Home = () => {
  return (
    <div>
       <Helmet>
        <title>Fitness || Home</title>
       
      </Helmet>

        <Banner />
        <Features />
        <About />
        <NewsLetterSection />  
        <BlogSection />
        <Trainer />
    </div>
  )
}

export default Home