import React from 'react'
import useAdmin from '../hooks/useAdmin'
import useAuth from '../hooks/useAuth'
import { Navigate, useLocation } from 'react-router-dom'

const AdminRoutes = ({children}) => {
    const [isAdmin ,isAdminLoading]= useAdmin()
    const {user ,isLoading} = useAuth()


    const location = useLocation()
    if (isLoading || isAdminLoading) {
      <progress className="progress w-56"></progress>;
    }
    // isLoading &&  !user?.email
    if(!isLoading && !isAdminLoading && user?.email && isAdmin){
     return children
   
    }
    return <Navigate to='/login' state={location.pathname} />
  
}

export default AdminRoutes