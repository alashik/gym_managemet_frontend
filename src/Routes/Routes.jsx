
import { createBrowserRouter } from "react-router-dom";

import MainLayout from "../Layout/MainLayout";
import Home from "../Home/Home";
import Login from "../pages/Login";
import Register from "../pages/Register";
import Trainer from "../pages/Trainer";
import TrainerDetailsPage from "../pages/Trainer/TrainerDetailsPage";
import PlansAndBooking from "../pages/PlansAndBooking";
import BeaTrainer from "../pages/BeaTrainer";
import PrivateRoute from './PrivateRoute';
import DailyClassShedule from "../pages/DailyClassShedule";
import DashBoard from "../Layout/DashBoard";
import AllUsers from "../pages/Dashboard/AllUsers";
import AllTrainer from "../pages/Dashboard/AllTrainer";
import ApplyTRainer from "../pages/Dashboard/ApplyTRainer";
import NotFound from "../components/NotFound/NotFound";
import AdminRoutes from "./AdminRoutes";
import Payment from "../pages/Payment/Payment";
import PaymentHistory from "../pages/Payment/PaymentHistory";
import AdminHome from "../pages/AdminHome/AdminHome";

const router = createBrowserRouter([
  {
    path: "/",
    element:  <MainLayout />,
    children: [
      {
        path: "/",
        element: <Home />,
      },
      
      {
        path: "/login",
        element: < Login />,
      },
  
      {
        path: "/register",
        element: < Register />,
      },
      {
        path: "/trainer",
        element: < Trainer />,
      },
      {
        path: "/trainer/:id",
        element:<PrivateRoute>  <TrainerDetailsPage /></PrivateRoute>,
        loader:({params})=> fetch(`https://fitness-backend-gamma.vercel.app/trainer/${params.id}`)
      },
      {
        path: "/bookings",
        element: <PlansAndBooking />,
        
      },
      {
        path: "/be-a-trainer",
        element: <BeaTrainer  />,
        
      },
      {
        path: "/classes",
        element: <DailyClassShedule  />,
        
      },
     
      {
        path: "/*",
        element: <NotFound   />,
        
      },
     
    ],
  },
  {
    path :'/dashboard',
    element :<PrivateRoute> <DashBoard /></PrivateRoute>,
    children:[
      {
        path: "admin-home",
        element: <AdminRoutes>  <AdminHome/> </AdminRoutes>   
      },
      {
        path: "payment",
        element:  <Payment />    
      },
      {
        path: "history",
        element: <PaymentHistory />    
      },
      {
        path: "users",
        element: <AdminRoutes> <AllUsers  />  </AdminRoutes>  
      },
      {
        path: "all-trainers",
        element: <AdminRoutes> <AllTrainer  />  </AdminRoutes>  
      },
      {
        path: "applyTrainer",
        element: <AdminRoutes> <ApplyTRainer  />  </AdminRoutes>  
      }
    ]
    
  }
]);

export default router ;